import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamTest {
    public static void main(String[] args) {
        FileOutputStream file = null;

        try {
            file = new FileOutputStream("FileOutputStream/src/main/resources/test", true);
            String msg = "I am Jason!\n";
            file.write(msg.getBytes());

            //写完后一定要刷新
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (file != null){
                try {
                    file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
