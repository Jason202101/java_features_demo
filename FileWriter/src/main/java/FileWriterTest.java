import java.io.FileWriter;
import java.io.IOException;

public class FileWriterTest {
    public static void main(String[] args) {
        FileWriter fwr = null;

        try {
            fwr = new FileWriter("FileWriter/src/main/resources/test", true);
            char[] chars = {'就', 'j', '9', 'g', '法'};
            fwr.write(chars);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fwr != null){
                try {
                    fwr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
