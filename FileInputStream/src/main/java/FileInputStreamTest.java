import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamTest {
    public static void main(String[] args) {
        FileInputStream files = null;

        try {
            files = new FileInputStream("FileInputStream/src/main/resources/test.txt");
            byte[] bytes = new byte[8];
            int count = files.read(bytes);
            while(count != -1){
                System.out.print(new String(bytes, 0, count));
                count = files.read(bytes);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(files != null){
                try {
                    files.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
