import java.io.FileReader;
import java.io.IOException;

public class FileReaderTest {
    public static void main(String[] args) {
        FileReader fir = null;

        try {
            fir = new FileReader("FileReader/src/main/resources/test");
            char[] chars = new char[10000];
            int count = 0;
            while ((count = fir.read(chars)) != -1){
                System.out.print(chars);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                if(fir != null){
                    fir.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
