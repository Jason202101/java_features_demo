import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileCopyWriterReader {
    public static void main(String[] args) {
        FileReader frd = null;
        FileWriter fwt = null;

        try {
            frd = new FileReader("FileCopy_FileWriter_FileReader/src/main/resources/test");
            fwt = new FileWriter("FileCopy_FileWriter_FileReader/src/main/resources/test2", true);
            int count = 0;
            char[] chars = new char[10];
            while((count = frd.read(chars)) != -1){
                fwt.write(chars, 0, count);
            }
            fwt.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(frd != null){
                try {
                    frd.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if(fwt != null){
            try {
                fwt.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
