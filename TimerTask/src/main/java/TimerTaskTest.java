import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class TimerTaskTest {
    public static void main(String[] args) throws ParseException {
        //Timer timer = new Timer(true); //开启守护线程
        Timer timer = new Timer(); //不开启守护线程

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse("2022-4-4 22:04:00");

        //匿名内部类方式
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println("定时任务执行");
//            }
//        }, date, 1000*10);

        //外部类继承方式
        timer.schedule(new TimeerTaskLog(), date, 1000*10);
    }
}

class TimeerTaskLog extends TimerTask{

    @Override
    public void run() {
        System.out.println("定时任务执行2");
    }
}