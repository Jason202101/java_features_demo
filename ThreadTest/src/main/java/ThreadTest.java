public class ThreadTest {
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.start();
        Thread myThread2 = new Thread(new MyThread2());
        myThread2.start();
        Thread myThread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    System.out.println("匿名内部类线程输出： " + i);
                }
            }
        });
        myThread3.start();
        for (int i = 0; i < 10000; i++){
            System.out.println("主线程输出： "+i);
        }
    }
}

class MyThread extends Thread{

    @Override
    public void run() {
        super.run();
        for (int i = 0; i < 10000; i++){
            System.out.println("子线程输出： "+i);
        }
    }
}

class MyThread2 implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++){
            System.out.println("2子线程输出： "+i);
        }
    }
}
